FROM openjdk:13-alpine
VOLUME /tmp
ADD /target/*.jar test-ci-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/test-ci-0.0.1-SNAPSHOT.jar"]